package com.IPL;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Main {

    public static Map<String, Integer> getMatchesPlayedPerYear(List<Match> matchesData) {
        Map<String, Integer> matchesPlayedPerYear = new HashMap<String, Integer>();
        for (Match match : matchesData) {
            if (matchesPlayedPerYear.containsKey(match.getSeason())) {
                matchesPlayedPerYear.put(match.getSeason(), matchesPlayedPerYear.get(match.getSeason()) + 1);
            } else {
                matchesPlayedPerYear.put(match.getSeason(), 1);
            }

        }
        return matchesPlayedPerYear;
    }

    public static Map<String, Integer> getMatchWonPerTeam(List<Match> matchesData) {

        Map<String, Integer> matchWonPerTeam = new HashMap<String, Integer>();
        for (Match match : matchesData) {
            if (matchWonPerTeam.containsKey(match.getWinner())) {
                matchWonPerTeam.put(match.getWinner(), matchWonPerTeam.get(match.getWinner()) + 1);
            } else {
                matchWonPerTeam.put(match.getWinner(), 1);
            }
        }
        return matchWonPerTeam;
    }

    public static Map<String, Integer> getExtraRunConcededPerTeam(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Integer> extraRunConcededPerTeam = new HashMap<String, Integer>();
        ArrayList<String> arrayListIDSeason2016 = new ArrayList<String>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2016")) {
                arrayListIDSeason2016.add(String.valueOf(match.getMatchID()));
            }
        }
        for (Delivery delivery : deliveriesData) {
            if (arrayListIDSeason2016.contains(String.valueOf(delivery.getDeliveryMatchId())) && !(delivery.getExtraRuns() == 0)) {
                if (extraRunConcededPerTeam.containsKey(delivery.getBowlingTeam())) {
                    extraRunConcededPerTeam.put(delivery.getBowlingTeam(), (extraRunConcededPerTeam.get(delivery.getBowlingTeam())) + delivery.getExtraRuns());
                } else {
                    extraRunConcededPerTeam.put(delivery.getBowlingTeam(), delivery.getExtraRuns());
                }
            }
        }
        return extraRunConcededPerTeam;
    }

    private static Map<String, Float> getTopTenEconomicalBowlers(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Float> economicalBowler = new HashMap<String, Float>();
        ArrayList<String> arrayListIDSeason2015 = new ArrayList<String>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2015")) {
                arrayListIDSeason2015.add(String.valueOf(match.getMatchID()));
            }
        }
//        System.out.println(arrayListIDSeason2015);
        ArrayList<Delivery> arrListDelivery2015 = new ArrayList<Delivery>();
        for (Delivery delivery : deliveriesData) {
            if (arrayListIDSeason2015.contains(String.valueOf(delivery.getDeliveryMatchId()))) {
//                System.out.println(delivery);
                arrListDelivery2015.add(delivery);
            }
        }
//        System.out.println(arrListDelivery2015);
        ArrayList<String> distinctBowlers = new ArrayList<String>();

        for (int index = 0; index < arrListDelivery2015.size(); index++) {
            if (!distinctBowlers.contains(arrListDelivery2015.get(index).getBowler())) {
                distinctBowlers.add(arrListDelivery2015.get(index).getBowler());
            }
        }
//        System.out.println(distinctBowlers);
        for (int i = 0; i < distinctBowlers.size(); i++) {
            economicalBowler.put(distinctBowlers.get(i), (float) calculateEconomicalBowler(distinctBowlers.get(i), arrListDelivery2015));
        }
        List<Map.Entry<String, Float>> list =
                new LinkedList<Map.Entry<String, Float>>(economicalBowler.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1,
                               Map.Entry<String, Float> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        HashMap<String, Float> topTenEconomicalBowlers = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> aa : list) {
            topTenEconomicalBowlers.put(aa.getKey(), aa.getValue());
        }
        return topTenEconomicalBowlers;
    }

    private static Float calculateEconomicalBowler(String player, ArrayList<Delivery> arrListDelivery2015) {
        ArrayList<Delivery> playerDeliveryData = new ArrayList<>();
        for (int index = 0; index < arrListDelivery2015.size(); index++) {
            if (arrListDelivery2015.get(index).getBowler().equals(player)) {
                playerDeliveryData.add(arrListDelivery2015.get(index));
            }
        }
        int runs = 0;
        int bowls = 0;
        for (int index = 0; index < playerDeliveryData.size(); index++) {
            runs += playerDeliveryData.get(index).getTotalRun();
            bowls++;
        }
        return runs / (bowls / 6F);
    }

    private static Map<String, Integer> getBoundriesOfEachPlayerIn2015(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Integer> boundriesOfPlayerIn2015 = new HashMap<String, Integer>();
        ArrayList<String> arrayListIDSeason2015 = new ArrayList<String>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2015")) {
                arrayListIDSeason2015.add(String.valueOf(match.getMatchID()));
            }
        }
//        System.out.println(arrayListIDSeason2015);
        for (Delivery delivery : deliveriesData) {
//            System.out.println(delivery);
            if (arrayListIDSeason2015.contains(Integer.toString(delivery.getDeliveryMatchId()))) {
                if (delivery.getBatsmanRun() == 4 || delivery.getBatsmanRun() == 6) {
                    if (boundriesOfPlayerIn2015.containsKey(delivery.getBatsman())) {
                        boundriesOfPlayerIn2015.put(delivery.getBatsman(), boundriesOfPlayerIn2015.get(delivery.getBatsman()) + 1);
                    } else {
                        boundriesOfPlayerIn2015.put(delivery.getBatsman(), 1);
                    }
                }
            }
        }
        return boundriesOfPlayerIn2015;
    }

    private static Map<String, Integer> getWicketTakenByBumrah2014(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Integer> wicketsTakenByBumrah = new HashMap<String, Integer>();
        wicketsTakenByBumrah.put("JJ Bumrah", 0);
        ArrayList<String> arrayListIDSeason2014 = new ArrayList<String>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2014")) {
                arrayListIDSeason2014.add(String.valueOf(match.getMatchID()));
            }
        }
//        System.out.println(arrayListIDSeason2014);
        for (Delivery delivery : deliveriesData) {
            if (arrayListIDSeason2014.contains(Integer.toString(delivery.getDeliveryMatchId()))) {
                if (delivery.getBowler().equals("JJ Bumrah") && !delivery.getPlayerDismissed().equals("")) {
                    wicketsTakenByBumrah.put("JJ Bumrah", wicketsTakenByBumrah.get("JJ Bumrah") + 1);
                }
            }
        }
        return wicketsTakenByBumrah;
    }

    private static Map<String, Integer> getMatchesHeldInBangalorStadium2012(List<Match> matchesData) {
        Map<String, Integer> matchesHeldInBangalorStadium = new HashMap<String, Integer>();
        matchesHeldInBangalorStadium.put("Sawai Mansingh Stadium", 0);
        for (Match match : matchesData) {
            if (match.getSeason().equals("2014")) {
                if (match.getVenue().equals("Sawai Mansingh Stadium")) {
                    matchesHeldInBangalorStadium.put("Sawai Mansingh Stadium", matchesHeldInBangalorStadium.get("Sawai Mansingh Stadium") + 1);
                }
            }

        }
//        System.out.println(arrayListIDSeason2014);
        return matchesHeldInBangalorStadium;
    }

    private static Map<String, Integer> getExtraRunConcededInSuperOver2017(List<Match> matchesData, List<Delivery> deliveriesData) {
        Map<String, Integer> extraRunConcededInSuperOver = new HashMap<String, Integer>();
        ArrayList<String> arrayListIDSeason2017 = new ArrayList<String>();
        for (Match match : matchesData) {
            if (match.getSeason().equals("2017")) {
                arrayListIDSeason2017.add(String.valueOf(match.getMatchID()));
            }
        }
//        System.out.println(arrayListIDSeason2017);
        for (Delivery delivery : deliveriesData) {
            if (!(delivery.getIsSuperOver() == 0) && !(delivery.getExtraRuns() == 0)) {
                if (arrayListIDSeason2017.contains(Integer.toString(delivery.getDeliveryMatchId()))) {
                    if (extraRunConcededInSuperOver.containsKey(delivery.getBowlingTeam())) {
                        extraRunConcededInSuperOver.put(delivery.getBowlingTeam(), (extraRunConcededInSuperOver.get(delivery.getBowlingTeam())) + delivery.getExtraRuns());
                    } else {
                        extraRunConcededInSuperOver.put(delivery.getBowlingTeam(), delivery.getExtraRuns());
                    }
                }
            }
        }
        return extraRunConcededInSuperOver;
    }


    private static Map<String, Integer> getNumberOfMatchesCSKWonInKingsmed(List<Match> matchesData) {
        Map<String, Integer> numberOfMatchesWonCount = new HashMap<String, Integer>();

        for (Match match : matchesData) {
//            System.out.println(match);
            if (match.getVenue().equals("Kingsmead")) {
                if (numberOfMatchesWonCount.containsKey(match.getWinner())) {
                    numberOfMatchesWonCount.put(match.getWinner(), numberOfMatchesWonCount.get(match.getWinner()) + 1);
                } else {
                    numberOfMatchesWonCount.put(match.getWinner(), 1);
                }
            }
        }
        return numberOfMatchesWonCount;
    }

    private static Map<String, String> getHighestNumberOfPlayerOfTheMatch(List<Match> matchesData) throws IOException {
        ArrayList<String> distinctSeason = new ArrayList<String>();
        ArrayList<Match> matcheData = new ArrayList<>();
        Map<String, String> highestNumberOfPlayerOfTheMatchs = new HashMap<String, String>();

        for (Match match : matchesData) {
            matcheData.add(match);
            if (!distinctSeason.contains(match.getSeason())) {
                distinctSeason.add(match.getSeason());
            }
        }
//        System.out.println(distinctSeason);
        for (int i = 0; i < distinctSeason.size(); i++) {
            highestNumberOfPlayerOfTheMatchs.put(distinctSeason.get(i), countPlayerAwarded(distinctSeason.get(i), matcheData));
        }
        return highestNumberOfPlayerOfTheMatchs;
    }

    private static String countPlayerAwarded(String season, ArrayList<Match> matchesData) throws IOException {
        Map<String, Integer> playerOfMatch = new HashMap<String, Integer>();
        for (Match matchData : matchesData) {
//            System.out.println(season);
//            System.out.println(matchData.getSeason());
            if (season.equals(matchData.getSeason())) {
                if (playerOfMatch.containsKey(matchData.getPlayerOfMatch())) {
                    playerOfMatch.put(matchData.getPlayerOfMatch(), playerOfMatch.get(matchData.getPlayerOfMatch()) + 1);
                } else {
                    playerOfMatch.put(matchData.getPlayerOfMatch(), 1);
                }
            }
        }

//        System.out.println(playerOfMatch);
        int max = -1;
        String player = "";
        for (Map.Entry<String, Integer> players : playerOfMatch.entrySet()) {
            if (players.getValue() > max) {
                max = players.getValue();
                player = players.getKey();
            }
        }
//        System.out.println(player);
        return player;
    }

    public static void main(String[] args) {
        String line = "";
        String splitBy = ",";
        List<Match> matchesData = new ArrayList<Match>();
        List<Delivery> deliveriesData = new ArrayList<Delivery>();
        try {
            String MatchesData = "Java-IPL-Project/data/matches.csv";
            String DeliveriesData = "Java-IPL-Project/data/deliveries.csv";
            BufferedReader brMatchData = new BufferedReader(new FileReader(MatchesData));
            BufferedReader brDeliveryData = new BufferedReader(new FileReader(DeliveriesData));
//            System.out.println(brMatchData);
            brMatchData.readLine();
            brDeliveryData.readLine();
            while ((line = brMatchData.readLine()) != null) {
                String[] matchData = line.split(splitBy);
                Match matches = new Match();
                matches.setMatchID(Integer.parseInt(matchData[0]));
                matches.setSeason(matchData[1]);
                matches.setWinner(matchData[10]);
                matches.setPlayerOfMatch(matchData[13]);
                matches.setVenue(matchData[14]);
                matchesData.add(matches);
            }
            while ((line = brDeliveryData.readLine()) != null) {
                String[] deliveryData = line.split(splitBy);
                Delivery deliveries = new Delivery();
                deliveries.setDeliveryMatchId(Integer.parseInt(deliveryData[0]));
                deliveries.setExtraRuns(Integer.parseInt(deliveryData[16]));
                deliveries.setBowlingTeam(deliveryData[3]);
                deliveries.setBatsmanRun(Integer.parseInt(deliveryData[15]));
                deliveries.setBatsman(deliveryData[6]);
                deliveries.setBowler(deliveryData[8]);
                deliveries.setTotalRun(Integer.parseInt(deliveryData[17]));
                deliveries.setIsSuperOver(Integer.parseInt(deliveryData[9]));

                if (deliveryData.length > 18) {
                    deliveries.setPlayerDismissed(deliveryData[18]);
                } else {
                    deliveries.setPlayerDismissed("");
                }
                deliveriesData.add(deliveries);
            }
//            System.out.println(matchesData);
//            System.out.println(deliveriesData);

            System.out.println("1. Number of matches played per year of all the years in IPL.");
            System.out.println(getMatchesPlayedPerYear(matchesData));
            System.out.println("2. Number of matches won of all teams over all the years of IPL.");
            System.out.println(getMatchWonPerTeam(matchesData));
            System.out.println("3. For the year 2016 get the extra runs conceded per team.");
            System.out.println(getExtraRunConcededPerTeam(matchesData, deliveriesData));
            System.out.println("4. For the year 2015 get the top economical bowlers.");
            System.out.println(getTopTenEconomicalBowlers(matchesData, deliveriesData));
            System.out.println("5. For the year 2015 get the boundries of each player.");
            System.out.println(getBoundriesOfEachPlayerIn2015(matchesData, deliveriesData));
            System.out.println("6. Wickets of JJ Bumrah in 2014");
            System.out.println(getWicketTakenByBumrah2014(matchesData, deliveriesData));
            System.out.println("7. Count number of matches held in banglore stadium in 2012 ");
            System.out.println(getMatchesHeldInBangalorStadium2012(matchesData));
            System.out.println("8. Extra runs conceded by team in super over for season 2017");
            System.out.println(getExtraRunConcededInSuperOver2017(matchesData, deliveriesData));
            System.out.println("9. Find a player who has won the highest number of Player of the Match awards for each season");
            System.out.println(getHighestNumberOfPlayerOfTheMatch(matchesData));
            System.out.println("10. Count the number  of matches teams have won in kingsmead.");
            System.out.println(getNumberOfMatchesCSKWonInKingsmed(matchesData));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
