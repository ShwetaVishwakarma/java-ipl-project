package com.IPL;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Ipl {

    public static void matchesPerYear(BufferedReader brMatch) throws IOException {
        String line = "";
        String splitBy = ",";
        int count = 0;
        Map<String, Integer> matchesPlayedPerYear = new HashMap<String, Integer>();

        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);    // use comma as separator
            if (count > 0) {
                if (matchesPlayedPerYear.containsKey(matchData[1])) {
                    matchesPlayedPerYear.put(matchData[1], matchesPlayedPerYear.get(matchData[1]) + 1);
                } else {
                    matchesPlayedPerYear.put(matchData[1], 1);
                }
            }
            count++;
        }
        System.out.println("1. Number of matches played per year of all the years in IPL.");
        System.out.println(matchesPlayedPerYear);
    }

    public static void matchesWonPerTeam(BufferedReader brMatch) throws IOException {
        String line = "";
        String splitBy = ",";
        int count = 0;
        Map<String, Integer> matchWonPerTeam = new HashMap<String, Integer>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (count > 0) {
                if (matchWonPerTeam.containsKey(matchData[10])) {
                    matchWonPerTeam.put(matchData[10], matchWonPerTeam.get(matchData[10]) + 1);
                } else {
                    matchWonPerTeam.put(matchData[10], 1);
                }
            }
            count++;
        }
        System.out.println("2. Number of matches won of all teams over all the years of IPL.");
        System.out.println(matchWonPerTeam);
    }

    public static void extraRunConcededPerTeam2016(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Integer> extraRunConcededPerTeam = new HashMap<String, Integer>();
        ArrayList<String> arrli = new ArrayList<String>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (matchData[1].equals("2016")) {
                arrli.add(matchData[0]);
            }
        }
        int count = 0;
        while ((line = brDelivery.readLine()) != null) {
            String[] deliveryData = line.split(splitBy);
            if (count > 0) {
                if (arrli.contains(deliveryData[0]) && !deliveryData[16].equals("0")) {
                    if (extraRunConcededPerTeam.containsKey(deliveryData[3])) {
                        extraRunConcededPerTeam.put(deliveryData[3], (extraRunConcededPerTeam.get(deliveryData[3])) + Integer.parseInt(deliveryData[16]));
                    } else {
                        extraRunConcededPerTeam.put(deliveryData[3], Integer.parseInt(deliveryData[16]));
                    }
                }
            }
            count++;
        }
        System.out.println("3. For the year 2016 get the extra runs conceded per team.");
        System.out.println(extraRunConcededPerTeam);
    }

    public static void topTenEconomicalBowlers(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Float> economicalBowler = new HashMap<String, Float>();
        ArrayList<String> arrList = new ArrayList<String>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (matchData[1].equals("2015")) {
                arrList.add(matchData[0]);
            }
        }
        ArrayList<String[]> arrListDelivery2015 = new ArrayList<String[]>();
        while ((line = brDelivery.readLine()) != null) {
            String[] deliveryData = line.split(splitBy);
            if (arrList.contains(deliveryData[0])) {
                arrListDelivery2015.add(deliveryData);
            }
        }
        //
        // System.out.println(arrListDelivery.get(0)[0]);
        ArrayList<String> distinctBowlers = new ArrayList<String>();

        for (int index = 0; index < arrListDelivery2015.size(); index++) {
            if (!distinctBowlers.contains(arrListDelivery2015.get(index)[8])) {
                distinctBowlers.add(arrListDelivery2015.get(index)[8]);
            }
        }

        for (int i = 0; i < distinctBowlers.size(); i++) {
            economicalBowler.put(distinctBowlers.get(i), (float) calculateEconomicalBowler(distinctBowlers.get(i), arrListDelivery2015));
        }


        List<Map.Entry<String, Float>> list =
                new LinkedList<Map.Entry<String, Float>>(economicalBowler.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Float>>() {
            public int compare(Map.Entry<String, Float> o1,
                               Map.Entry<String, Float> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        HashMap<String, Float> topTenEconomicalBowlers = new LinkedHashMap<String, Float>();
        for (Map.Entry<String, Float> aa : list) {
            topTenEconomicalBowlers.put(aa.getKey(), aa.getValue());
        }
        System.out.println("4. For the year 2015 get the top economical bowlers.");
        System.out.println(topTenEconomicalBowlers);
    }

    private static Float calculateEconomicalBowler(String player, ArrayList<String[]> arrListDelivery2015) {
        // Create a list from elements of HashMap
        ArrayList<String[]> playerDeliveryData = new ArrayList<>();
        for (int index = 0; index < arrListDelivery2015.size(); index++) {
            if (arrListDelivery2015.get(index)[8].equals(player)) {
                playerDeliveryData.add(arrListDelivery2015.get(index));
            }
        }
        int runs = 0;
        int bowls = 0;
        for (int index = 0; index < playerDeliveryData.size(); index++) {
            runs += Integer.parseInt(playerDeliveryData.get(index)[17]);
            bowls++;
        }
        return runs / (bowls / 6F);
    }

    public static void boundriesOfEachPlayerIn2015(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Integer> boundriesOfPlayerIn2015 = new HashMap<String, Integer>();
        ArrayList<String> arrayList = new ArrayList<String>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (matchData[1].equals("2015")) {
                arrayList.add(matchData[0]);
            }
        }
        int count = 0;
        while ((line = brDelivery.readLine()) != null) {
            String[] deliveryData = line.split(splitBy);
            if (count > 0) {
                if (arrayList.contains(deliveryData[0])) {
                    if (deliveryData[15].equals("4") || deliveryData[15].equals("6")) {
                        if (boundriesOfPlayerIn2015.containsKey(deliveryData[6])) {
                            boundriesOfPlayerIn2015.put(deliveryData[6], boundriesOfPlayerIn2015.get(deliveryData[6]) + 1);
                        } else {
                            boundriesOfPlayerIn2015.put(deliveryData[6], 1);
                        }
                    }
                }
            }
            count++;
        }
        System.out.println("5. For the year 2015 get the boundries of each player.");
        System.out.println(boundriesOfPlayerIn2015);
    }

    public static void wicketTakenByBumrah2014(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Integer> wicketsTakenByBumrah = new HashMap<String, Integer>();
        wicketsTakenByBumrah.put("JJ Bumrah", 0);

        ArrayList<String> arrayList = new ArrayList<String>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (matchData[1].equals("2014")) {
                arrayList.add(matchData[0]);
            }
        }
        int count = 0;
        while ((line = brDelivery.readLine()) != null) {
            String[] deliveryData = line.split(splitBy);
            if (count > 0 && deliveryData.length > 18) {
                if (arrayList.contains(deliveryData[0])) {
                    if (deliveryData[8].equals("JJ Bumrah") && !deliveryData[18].equals("")) {
                        wicketsTakenByBumrah.put("JJ Bumrah", wicketsTakenByBumrah.get("JJ Bumrah") + 1);
                    }
                }
            }
            count++;
        }
        System.out.println("6. Wickets of JJ Bumrah in 2014");
        System.out.println(wicketsTakenByBumrah);
    }

    public static void matchesHeldInBanglorStadium2012(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Integer> matchesHeldInBangalorStadium = new HashMap<String, Integer>();
        matchesHeldInBangalorStadium.put("Sawai Mansingh Stadium", 0);
        int count = 0;
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (count > 0) {
                if (matchData[1].equals("2012")) {
                    if (matchData[14].equals("Sawai Mansingh Stadium")) {
                        matchesHeldInBangalorStadium.put("Sawai Mansingh Stadium", matchesHeldInBangalorStadium.get("Sawai Mansingh Stadium") + 1);
                    }
                }
            }
            count++;
        }
        System.out.println("7. Count number of matches held in banglore stadium in 2012 ");
        System.out.println(matchesHeldInBangalorStadium);
    }

    public static void extraRunConcededInSuperOver2017(BufferedReader brMatch, BufferedReader brDelivery) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, Integer> extraRunConcededInSuperOver = new HashMap<String, Integer>();

        ArrayList<String> arrayList = new ArrayList<String>();
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            if (matchData[1].equals("2017")) {
                arrayList.add(matchData[0]);
            }
        }
        int count = 0;
        while ((line = brDelivery.readLine()) != null) {
            String[] deliveryData = line.split(splitBy);
            if (count > 0) {
                if (!deliveryData[9].equals("0") && !deliveryData[16].equals("0")) {
                    if (arrayList.contains(deliveryData[0])) {
                        if (extraRunConcededInSuperOver.containsKey(deliveryData[3])) {
                            extraRunConcededInSuperOver.put(deliveryData[3], (extraRunConcededInSuperOver.get(deliveryData[3])) + Integer.parseInt(deliveryData[16]));
                        } else {
                            extraRunConcededInSuperOver.put(deliveryData[3], Integer.parseInt(deliveryData[16]));
                        }
                    }
                }
            }
            count++;
        }

        System.out.println("8. Extra runs conceded by team in super over for season 2017");
        System.out.println(extraRunConcededInSuperOver);
    }

    public static void highestNumberOfPlayerOfTheMatch(BufferedReader brMatch) throws IOException {
        String line = "";
        String splitBy = ",";
        Map<String, String> highestNumberOfPlayerOfTheMatchs = new HashMap<String, String>();

        ArrayList<String> distinctSeason = new ArrayList<String>();
        ArrayList<String[]> matchesData = new ArrayList<String[]>();

        int count = 0;
        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);
            matchesData.add(matchData);

            if (count > 0) {
                for (int index = 0; index < matchData.length; index++) {
                    if (!distinctSeason.contains(matchData[1])) {
                        distinctSeason.add(matchData[1]);
                    }
                }
            }
            count++;
        }
        for (int i = 0; i < distinctSeason.size(); i++) {
            highestNumberOfPlayerOfTheMatchs.put(distinctSeason.get(i),countPlayerAwarded(distinctSeason.get(i), matchesData));
        }
        System.out.println(highestNumberOfPlayerOfTheMatchs);
    }

    private static String countPlayerAwarded(String season, ArrayList<String[]> matchesData) throws IOException {

        int count = 0;
        Map<String, Integer> playerOfMatch = new HashMap<>();
//        System.out.println(matchesData);
        for (String[] matchData : matchesData) {
            if (season.equals(matchData[1])) {
                if (playerOfMatch.containsKey(matchData[13])) {
//                    System.out.println(playerOfMatch.get(csvMatchData[13]));

                    playerOfMatch.put(matchData[13], playerOfMatch.get(matchData[13]) + 1);

                } else {
                    playerOfMatch.put(matchData[13], 1);
                }
            }
        }
        int max = -1;
        String player = "";
        for (Map.Entry<String, Integer> players : playerOfMatch.entrySet()) {
            if (players.getValue() > max) {
                max = players.getValue();
                player = players.getKey();
            }
        }
        return player;
    }

    public static void numberOfMatchesCSKWonInKingsmed(BufferedReader brMatch) throws IOException {
        String line = "";
        String splitBy = ",";
        int count = 0;
        Map<String, Integer> numberOfMatchesWonCount = new HashMap<String, Integer>();


        while ((line = brMatch.readLine()) != null)   //returns a Boolean value
        {
            String[] matchData = line.split(splitBy);    // use comma as separator
            if (count > 0) {
                if(matchData[14].equals("Kingsmead")){
                    if (numberOfMatchesWonCount.containsKey(matchData[10])) {
                        numberOfMatchesWonCount.put(matchData[10], numberOfMatchesWonCount.get(matchData[10]) + 1);
                    } else {
                        numberOfMatchesWonCount.put(matchData[10], 1);
                    }
                }
            }
            count++;
        }
        System.out.println("10. Count the number  of matches teams have won in kingsmead.");
        System.out.println(numberOfMatchesWonCount);
    }

    public static void main(String args[]) {
        {
            String line = "";
            String splitBy = ",";
            try {
                String MatchesData = "Java-IPL-Project/data/matches.csv";
                String DeliveriesData = "Java-IPL-Project/data/deliveries.csv";
                //parsing a CSV file into BufferedReader class constructor
                BufferedReader brMatch = new BufferedReader(new FileReader(MatchesData));
                BufferedReader brDelivery = new BufferedReader(new FileReader(DeliveriesData));
//                matchesPerYear(brMatch);
//                matchesWonPerTeam(brMatch);
//                extraRunConcededPerTeam2016(brMatch, brDelivery);
//                topTenEconomicalBowlers(brMatch, brDelivery);
//                boundriesOfEachPlayerIn2015(brMatch, brDelivery);
//                wicketTakenByBumrah2014(brMatch, brDelivery);
//                matchesHeldInBanglorStadium2012(brMatch, brDelivery);
//                extraRunConcededInSuperOver2017(brMatch, brDelivery);
                highestNumberOfPlayerOfTheMatch(brMatch);
//                numberOfMatchesCSKWonInKingsmed(brMatch);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
