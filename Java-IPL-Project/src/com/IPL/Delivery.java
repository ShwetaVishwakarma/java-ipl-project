package com.IPL;

public class Delivery { 
    private int deliveryMatchId;
private int extraRuns;
private String bowlingTeam;
private int batsmanRun;
private String batsman;
private String bowler;
private String playerDismissed;
private int totalRun;
private  int isSuperOver;

    public int getIsSuperOver() {
        return isSuperOver;
    }

    public void setIsSuperOver(int isSuperOver) {
        this.isSuperOver = isSuperOver;
    }

    public int getTotalRun() {
        return totalRun;
    }

    public void setTotalRun(int totalRun) {
        this.totalRun = totalRun;
    }

    public int getDeliveryMatchId() {
        return deliveryMatchId;
    }

    public void setDeliveryMatchId(int deliveryMatchId) {
        this.deliveryMatchId = deliveryMatchId;
    }

    public int getExtraRuns() {
        return extraRuns;
    }

    public void setExtraRuns(int extraRuns) {
        this.extraRuns = extraRuns;
    }

    public String getBowlingTeam() {
        return bowlingTeam;
    }

    public void setBowlingTeam(String bowlingTeam) {
        this.bowlingTeam = bowlingTeam;
    }

    public int getBatsmanRun() {
        return batsmanRun;
    }

    public void setBatsmanRun(int batsmanRun) {
        this.batsmanRun = batsmanRun;
    }

    public String getBatsman() {
        return batsman;
    }

    public void setBatsman(String batsman) {
        this.batsman = batsman;
    }

    public String getBowler() {
        return bowler;
    }

    public void setBowler(String bowler) {
        this.bowler = bowler;
    }

    public String getPlayerDismissed() {
        return playerDismissed;
    }

    public void setPlayerDismissed(String playerDismissed) {
        this.playerDismissed = playerDismissed;
    }
}
